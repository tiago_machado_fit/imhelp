"""imelhp URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include

from donations.views import index, DonationTypeListView, DonationTypeUpdateView, DonationTypeDeleteView, DontationTypeCreateView, DonationProductListView, DonationProductUpdateView, DonationProductDeleteView, DonationProductCreateView, DonationMeasureListView, DonationMeasureUpdateView, DonationMeasureDeleteView, DonationMeasureCreateView, DonationListView, DonationUpdateView, DonationDeleteView, DonationCreateView, DonationDetailView, DonationItemUpdateView, DonationItemCreateView, DonationItemDeleteView, DonationProductGroupListView, DonationProductGroupUpdateView, DonationProductGroupDeleteView, DonationProductGroupCreateView

urlpatterns = [
    # Doacao
    path('', DonationListView.as_view(), name='doacoes'),
    path('<int:donation_pk>/', DonationDetailView.as_view(), name='donation_detail'),
    path('<int:donation_pk>/editar', DonationUpdateView.as_view(), name='donation_edit'),    
    path('<int:donation_pk>/apagar', DonationDeleteView.as_view(), name='donation_delete'),
    path('nova', DonationCreateView.as_view(), name='donation_create'),
    # Tipo
    path('tipo', DonationTypeListView.as_view(), name='tipo'),
    path('tipo/<int:donationtype_pk>/editar', DonationTypeUpdateView.as_view(), name='donationtype_edit'),
    path('tipo/<int:donationtype_pk>/apagar', DonationTypeDeleteView.as_view(), name='donationtype_delete'),
    path('tipo/novo', DontationTypeCreateView.as_view(), name='donationtype_create'),
    # Grupo Produto
    path('grupoproduto', DonationProductGroupListView.as_view(), name='grupoproduto'),
    path('grupoproduto/<int:donationproductgroup_pk>/editar', DonationProductGroupUpdateView.as_view(), name='donationproductgroup_edit'),
    path('grupoproduto/<int:donationproductgroup_pk>/apagar', DonationProductGroupDeleteView.as_view(), name='donationproductgroup_delete'),
    path('grupoproduto/novo', DonationProductGroupCreateView.as_view(), name='donationproductgroup_create'),
    # Produto
    path('produto', DonationProductListView.as_view(), name='produto'),
    path('produto/<int:donationproduct_pk>/editar', DonationProductUpdateView.as_view(), name='donationproduct_edit'),
    path('produto/<int:donationproduct_pk>/apagar', DonationProductDeleteView.as_view(), name='donationproduct_delete'),
    path('produto/novo', DonationProductCreateView.as_view(), name='donationproduct_create'),
    # Medida
    path('medida', DonationMeasureListView.as_view(), name='medida'),
    path('medida/<int:donationmeasure_pk>/editar', DonationMeasureUpdateView.as_view(), name='donationmeasure_edit'),
    path('medida/<int:donationmeasure_pk>/apagar', DonationMeasureDeleteView.as_view(), name='donationmeasure_delete'),
    path('medida/nova', DonationMeasureCreateView.as_view(), name='donationmeasure_create'),
    # Item de Doacao
    path('<int:donation_pk>/item/<int:donationitem_pk>/', DonationItemUpdateView.as_view(), name='donationitem_edit'),
    path('<int:donation_pk>/item/<int:donationitem_pk>/apagar', DonationItemDeleteView.as_view(), name='donationitem_delete'),
    path('<int:donation_pk>/item/novo', DonationItemCreateView.as_view(), name='donationitem_create'),
]
