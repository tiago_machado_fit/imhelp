# Generated by Django 3.1.3 on 2020-12-02 22:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('donations', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='donationproductgroup',
            name='name',
            field=models.CharField(max_length=30),
        ),
    ]
