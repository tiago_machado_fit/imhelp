from django.contrib import admin
from donations.models import DonationProduct, DonationMeasure, DonationType, Donation, DonationItem

# Register your models here.
admin.site.register(DonationProduct)
admin.site.register(DonationMeasure)
admin.site.register(DonationType)
admin.site.register(Donation)
admin.site.register(DonationItem)