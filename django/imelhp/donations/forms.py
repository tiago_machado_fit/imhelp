from django.utils.translation import gettext_lazy as _

from django import forms
from django.core.exceptions import ValidationError

from donations.models import DonationType, DonationProduct, DonationProductGroup, DonationMeasure, Donation, DonationItem


class DonationTypeForm(forms.ModelForm):

    name = forms.CharField(label = 'Tipo de Doação')
    add_to_stock = forms.BooleanField(label = 'Este tipo de doação adiciona itens doados ao estoque', required=False)
    
    class Meta:
        model = DonationType
        fields = ['name', 'add_to_stock']

class DonationProductGroupForm(forms.ModelForm):

    name = forms.CharField(label = 'Produto')
    
    class Meta:
        model = DonationProductGroup
        fields = ['name']

class DonationProductForm(forms.ModelForm):

    name = forms.CharField(label = 'Produto')
    group = forms.ModelChoiceField(label = 'Grupo de Produto', queryset = DonationProductGroup.objects.all())
    
    class Meta:
        model = DonationProduct
        fields = ['name', 'group']

class DonationMeasureForm(forms.ModelForm):

    name = forms.CharField(label = 'Medida')
    item = forms.ModelMultipleChoiceField(queryset=DonationProduct.objects.all().order_by('name'), label = 'Se aplica a')

    class Meta:
        model = DonationMeasure
        fields = ['name', 'item']

class DonationForm(forms.ModelForm):

    date = forms.DateField(label = 'Data')
    type = forms.ModelChoiceField(label = 'Tipo de Doação', queryset = DonationType.objects.all())
    comment = forms.CharField(label = 'Descrição')
    
    class Meta:
        model = Donation
        fields = ['date', 'type', 'comment']

class DonationItemForm(forms.ModelForm):

    product = forms.ModelChoiceField(label = 'Produto', queryset = DonationProduct.objects.all().order_by('name'))
    quantity = forms.FloatField(label = 'Quantidade', min_value=0.1)
    measure = forms.ModelChoiceField(label = 'Medida', queryset = DonationMeasure.objects.all().order_by('name'))
    
    def clean_measure(self):
        form_measure = self.cleaned_data['measure']
        if 'product' in self.cleaned_data:
            form_product = self.cleaned_data['product']
            valid = len(DonationMeasure.objects.filter(name=form_measure).filter(item=form_product)) > 0
            if not valid:
                raise ValidationError("Medida inválida para este produto")
        return form_measure

    class Meta:
        model = DonationItem
        fields = ['product', 'quantity', 'measure']