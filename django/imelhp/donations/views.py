from django.shortcuts import render
from django.urls import reverse_lazy
from django.http import JsonResponse
from django.core.exceptions import ValidationError
from django.db.models import Count, F, Q

from django.views.generic import ListView, UpdateView, DetailView, CreateView, DeleteView, DetailView

from donations.models import DonationType, DonationProduct, DonationProductGroup, DonationMeasure, Donation, DonationItem 
from donations.forms import DonationTypeForm, DonationProductForm, DonationProductGroupForm, DonationMeasureForm, DonationForm, DonationItemForm


# Dontation Type

class DonationTypeListView(ListView):
    model = DonationType
    context_object_name = 'DonationTypes'

class DonationTypeUpdateView(UpdateView):
    model = DonationType
    form_class = DonationTypeForm
    template_name = 'form.html'
    pk_url_kwarg = 'donationtype_pk'
    success_url = reverse_lazy('tipo')

class DonationTypeDeleteView(DeleteView):
    model = DonationType
    pk_url_kwarg = 'donationtype_pk'
    success_url = reverse_lazy('tipo')

class DontationTypeCreateView(CreateView):
    model = DonationType
    form_class = DonationTypeForm
    template_name = 'form.html'
    success_url = reverse_lazy('tipo')

# Donation Product Group

class DonationProductGroupListView(ListView):
    model = DonationProductGroup
    context_object_name = 'DonationProductGroups'

class DonationProductGroupUpdateView(UpdateView):
    model = DonationProductGroup
    form_class = DonationProductGroupForm
    template_name = 'form.html'
    pk_url_kwarg = 'donationproductgroup_pk'
    success_url = reverse_lazy('grupoproduto')

class DonationProductGroupDeleteView(DeleteView):
    model = DonationProductGroup
    pk_url_kwarg = 'donationproductgroup_pk'
    success_url = reverse_lazy('grupoproduto')

class DonationProductGroupCreateView(CreateView):
    model = DonationProductGroup
    template_name = 'form.html'
    form_class = DonationProductGroupForm
    success_url = reverse_lazy('grupoproduto')


# Donation Product

class DonationProductListView(ListView):
    model = DonationProduct
    context_object_name = 'DonationProducts'
    queryset = DonationProduct.objects.all().order_by('name')

class DonationProductUpdateView(UpdateView):
    model = DonationProduct
    form_class = DonationProductForm
    template_name = 'form.html'
    pk_url_kwarg = 'donationproduct_pk'
    success_url = reverse_lazy('produto')

class DonationProductDeleteView(DeleteView):
    model = DonationProduct
    pk_url_kwarg = 'donationproduct_pk'
    success_url = reverse_lazy('produto')

class DonationProductCreateView(CreateView):
    model = DonationProduct
    template_name = 'form.html'
    form_class = DonationProductForm
    success_url = reverse_lazy('produto')

# Donation Measure

class DonationMeasureListView(ListView):
    model = DonationMeasure
    queryset = DonationMeasure.objects.all().order_by('name')
    context_object_name = 'DonationMeasures'

class DonationMeasureUpdateView(UpdateView):
    model = DonationMeasure
    form_class = DonationMeasureForm
    template_name = 'form.html'
    pk_url_kwarg = 'donationmeasure_pk'
    success_url = reverse_lazy('medida')

class DonationMeasureDeleteView(DeleteView):
    model = DonationMeasure
    pk_url_kwarg = 'donationmeasure_pk'
    success_url = reverse_lazy('medida')

class DonationMeasureCreateView(CreateView):
    model = DonationMeasure
    template_name = 'form.html'
    form_class = DonationMeasureForm
    success_url = reverse_lazy('medida')

# Donation

class DonationListView(ListView):
    model = Donation
    context_object_name = 'Donations'
    queryset = Donation.objects.all().order_by('date')

class DonationUpdateView(UpdateView):
    model = Donation
    form_class = DonationForm
    template_name = 'donation_form.html'
    pk_url_kwarg = 'donation_pk'
    
    def get_success_url(self):
        pk = self.kwargs['donation_pk']
        return reverse_lazy('donation_detail', kwargs={'donation_pk': pk})


class DonationDetailView(DetailView):
    model = Donation
    pk_url_kwarg = 'donation_pk'

    def get_context_data(self, **kwargs):
        context = super(DonationDetailView, self).get_context_data(**kwargs)
        context['Donations'] = Donation.objects.all().order_by('date')        
        context['Items'] = DonationItem.objects.filter(donation = self.kwargs['donation_pk']).order_by('product')
        products = DonationProduct.objects.filter(pk__in = context['Items'].values('product'))
        # context['Groups'] = DonationProductGroup.objects.filter(pk__in = products.values('group')).order_by('name')
        print(DonationProductGroup.objects.filter(pk__in = products.values('group')).order_by('name'))
        context['Groups'] = DonationItem.objects.values('product__group__name').annotate(group_quantity=Count('product__group__name')).annotate(group=F('product__group__name')).values('group', 'group_quantity')
        print(context['Groups'])
        return context

class DonationDeleteView(DeleteView):
    model = Donation
    pk_url_kwarg = 'donation_pk'
    success_url = reverse_lazy('doacoes')
    
    def get_context_data(self, **kwargs):
        context = super(DonationDeleteView, self).get_context_data(**kwargs)
        context['Items'] = DonationItem.objects.filter(donation = self.kwargs['donation_pk'])
        return context

class DonationCreateView(CreateView):
    model = Donation
    template_name = 'donation_form.html'
    form_class = DonationForm
    success_url = reverse_lazy('doacoes')

# Donation Item

class DonationItemUpdateView(UpdateView):
    model = DonationItem
    template_name = 'form.html'
    pk_url_kwarg = 'donationitem_pk'
    form_class = DonationItemForm
    
    def get_success_url(self):
        pk = self.kwargs['donation_pk']
        return reverse_lazy('donation_detail', kwargs={'donation_pk': pk})

class DonationItemCreateView(CreateView):
    model = DonationItem
    template_name = 'form.html'    
    form_class = DonationItemForm

    def get_success_url(self):
        pk = self.kwargs['donation_pk']
        return reverse_lazy('donation_detail', kwargs={'donation_pk': pk})

    def form_valid(self, form):
        form.instance.donation = Donation.objects.get(pk = self.kwargs['donation_pk'])
        
        # Validando se este item jah existe para esta doacao e medida
        form_donation = form.instance.donation
        form_measure = form.instance.measure
        form_product = form.instance.product
        
        valid = len(DonationItem.objects.filter(donation = form_donation).filter(product = form_product).filter(measure = form_measure)) == 0
        if not valid:
            form.add_error(None, ValidationError(f"{form_product} em {form_measure} já foi doado"))
            return self.form_invalid(form)            
        else:
            return super(DonationItemCreateView, self).form_valid(form)

class DonationItemDeleteView(DeleteView):
    model = DonationItem
    pk_url_kwarg = 'donationitem_pk'

    def get_success_url(self):
        pk = self.kwargs['donation_pk']
        return reverse_lazy('donation_detail', kwargs={'donation_pk': pk})


def index(request):
    return render(request, 'home.html')