from django.db import models
import json

# Create your models here.

# Group of product: Cloth, food, ...
class DonationProductGroup(models.Model):
    name = models.CharField(max_length = 30)

    def __str__(self):
        return self.name


# The item of donation: Sugar, Money, Oil
class DonationProduct(models.Model):
    name = models.CharField(max_length = 50)
    group = models.ForeignKey(DonationProductGroup, on_delete = models.CASCADE)
    
    def __str__(self):
        return self.name

# Each donation item has one or more measures: Kg, L, ...
class DonationMeasure(models.Model):
    name = models.CharField(max_length = 20)
    item = models.ManyToManyField(DonationProduct)

    def __str__(self):
        return f'{self.name}'

# A Donation could be received (raise the stock) or delivered (lowers the stock)
class DonationType(models.Model):
    name = models.CharField(max_length = 20)
    add_to_stock = models.BooleanField(default = False)

    def __str__(self):
        if self.add_to_stock:
            operation = '(+)'
        else:
            operation = '(-)'
            
        return f'{self.name} {operation}'


# We have a donation with a date, a type and a list of DonationItems
class Donation(models.Model):
    date = models.DateField()
    type = models.ForeignKey(DonationType, on_delete = models.CASCADE)
    comment = models.CharField(max_length=200, default='')
    
    def __str__(self):
        return f'{self.date} - {self.comment} [{self.type}]'

# Finally, each donation is a list of items containing one product, with some measure and a quantity
class DonationItem(models.Model):
    product = models.ForeignKey(DonationProduct, on_delete = models.CASCADE)
    measure = models.ForeignKey(DonationMeasure, on_delete = models.CASCADE)
    quantity = models.FloatField()
    donation = models.ForeignKey(Donation, on_delete = models.CASCADE)
    
    def save(self, *args, **kwargs):
        self.quantity = round(self.quantity, 2)
        super(DonationItem, self).save(*args, **kwargs)
    
    def __str__(self):
        return f'{self.donation} - {self.quantity} {self.measure} de {self.product}'
    
    class Meta:
        unique_together = (("donation", "product", "measure"),)
